from django.shortcuts import render, redirect
from .models import Product, BusinessRegistration
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .forms import SignupForm, BusinessRegistrationForm, UpdateUserForm, ChangePasswordForm, ProductForm
from django.contrib.auth.decorators import login_required



# Create your views here.
def home(request):
    products = Product.objects.all()
    return render(request,'index.html',{'products':products})
def contacts(request):
    return render(request,'contacts.html')

def about(request):
    return render(request,'about.html')

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, ('You have been logged in!!!!'))
            return redirect('/')
        else:
            messages.success(request, ('There was an error please try again---'))
            return redirect('login')
    else:
        return render(request,'login.html')

def logout_user(request):
    logout(request)
    messages.success(request, ('You have been logged out successfully!!!!!!!'))
    return redirect('home')

def register_user(request):
    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, ('You have been Registered successfully!!!!!!!'))
            return redirect('/')
        else:
            messages.success(request, ('Try again!!!!!!!'))
            return redirect('register')
    else:
        form.fields['password1'].help_text = ""
        return render(request, 'register.html', {'form':form})
    
@login_required(login_url ='login')
def business_register(request):
    if request.method =="POST":
        forma = BusinessRegistrationForm(request.POST)
        if forma.is_valid():
            forma.save()
            return redirect('/')
    else:
        forma = BusinessRegistrationForm()
        return render(request, 'business.html', {'forma':forma})
    
def update_user(request):
    if request.user.is_authenticated:
        current_user = User.objects.get(id=request.user.id)
        user_form = UpdateUserForm(request.POST or None, instance=current_user)

        if user_form.is_valid():
            user_form.save()

            login(request, current_user)
            messages.success(request, 'User has been udated!!!!')
            return redirect('home')
        return render(request, 'update_user.html', {'user_form':user_form})
        
    else:
        messages.success(request, 'Try again!!!!')
        return redirect('home')
    

def update_password(request):
    if request.user.is_authenticated:
      current_user = request.user

      if request.method == "POST":
          form = ChangePasswordForm(current_user, request.POST)
          if form.is_valid():
              form.save()
              messages.success(request, 'your password has been update!!!')

          else:
              for error in list(form.errors.values()):
                  messages.error(request,error)
                  return redirect('update_password')

    

      else:
        form = ChangePasswordForm(current_user)
        form.fields['new_password1'].help_text = ""

        return render(request, 'update_password.html',{'form':form})
    
    else:
        messages.success(request, 'You must be logged in!!!!')
        return redirect('home')

        
@login_required(login_url ='login')
def product_add(request):
    if request.method =="POST":
        form = ProductForm(request.POST)
        if form.is_valid():
            return redirect('/')
            if request.user.BusinessRegistration:
                product = form.save
                product.user = request.user
                product.save()
            else:
                return render(request, 'product.html',{'form':form})
        else:
            return redirect('business')
    else:
        
        return render(request, 'product.html', )

from django.shortcuts import render
from django.http import JsonResponse
import json
import datetime
from .models import * 
from .utils import cookieCart, cartData, guestOrder
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import ShopSetupForm

def store(request):
	data = cartData(request)

	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	products = Product.objects.all()
	context = {'products':products, 'cartItems':cartItems}
	return render(request, 'store/store.html', context)


def cart(request):
	data = cartData(request)

	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	context = {'items':items, 'order':order, 'cartItems':cartItems}
	return render(request, 'store/cart.html', context)

def checkout(request):
	data = cartData(request)
	
	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	context = {'items':items, 'order':order, 'cartItems':cartItems}
	return render(request, 'store/checkout.html', context)

def updateItem(request):
	data = json.loads(request.body)
	productId = data['productId']
	action = data['action']
	print('Action:', action)
	print('Product:', productId)

	customer = request.user.customer
	product = Product.objects.get(id=productId)
	order, created = Order.objects.get_or_create(customer=customer, complete=False)

	orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)

	if action == 'add':
		orderItem.quantity = (orderItem.quantity + 1)
	elif action == 'remove':
		orderItem.quantity = (orderItem.quantity - 1)

	orderItem.save()

	if orderItem.quantity <= 0:
		orderItem.delete()

	return JsonResponse('Item was added', safe=False)

def processOrder(request):
	transaction_id = datetime.datetime.now().timestamp()
	data = json.loads(request.body)

	if request.user.is_authenticated:
		customer = request.user.customer
		order, created = Order.objects.get_or_create(customer=customer, complete=False)
	else:
		customer, order = guestOrder(request, data)

	total = float(data['form']['total'])
	order.transaction_id = transaction_id

	if total == order.get_cart_total:
		order.complete = True
	order.save()

	if order.shipping == True:
		ShippingAddress.objects.create(
		customer=customer,
		order=order,
		address=data['shipping']['address'],
		city=data['shipping']['city'],
		state=data['shipping']['state'],
		zipcode=data['shipping']['zipcode'],
		)

	return JsonResponse('Payment submitted..', safe=False)


# Import any other necessary models

def shop_setup_page(request):
    # Check if the request method is POST
    if request.method == 'POST':
        form = ShopSetupForm(request.POST)
        if form.is_valid():
            # Process form data and create a new shop for the user
            shop = form.save(commit=False)
            shop.owner = request.user  # Associate the current user with the shop
            shop.save()

            # Display a success message and redirect to the shop setup page
            messages.success(request, 'Shop setup completed successfully!')
            return redirect('shop_setup')
    else:
        # If the request method is GET, display the form
        form = ShopSetupForm()

    # Render the shop setup page with the form
    return render(request, 'shop_setup.html', {'form': form})

      
    

