
from django.urls import path
from .import views

urlpatterns = [
    
    path('', views.home,name ='home'),
    path('contacts/', views.contacts,name ='contacts'),
    path('about/', views.about,name ='about'),
    path('login/', views.login_user,name ='login'),
    path('logout/', views.logout_user,name ='logout'),
    path('update_user/', views.update_user,name ='update_user'),
    path('update_password/', views.update_password,name ='update_password'),
    path('register/', views.register_user,name ='register'),
    path('business/', views.business_register, name ='business'),
    path('product/', views.product_add, name ='product'),
	path('', views.store, name="store"),
	path('cart/', views.cart, name="cart"),
	path('checkout/', views.checkout, name="checkout"),
	path('update_item/', views.updateItem, name="update_item"),
	path('process_order/', views.processOrder, name="process_order"),
    path('shop', views.shop_setup_page, name='shop_setup'),

    ]