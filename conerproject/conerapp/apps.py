from django.apps import AppConfig


class ConerappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'conerapp'
